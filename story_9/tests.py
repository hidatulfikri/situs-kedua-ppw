from django.test import TestCase, Client
from django.urls import resolve
from .views import books, books_data, search
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
import time

class Story9Test(TestCase):

	# TES UNTUK BOOKS.HTML
	# Mengetes URL
	def books_url_exists(self):
		response = Client().get('books')
		self.assertEqual(response.status_code,200)

	# Mengetes URL menggunakan fungsi books()
	def using_books_func(self):
		found = resolve('books')
		self.assertEqual(found.func, books)

	# Mengetes URL
	def books_data_url_exists(self):
		response = Client().get('api/books')
		self.assertEqual(response.status_code,200)

	# Mengetes URL menggunakan fungsi books_data()
	def using_books_data_func(self):
		found = resolve('api/books')
		self.assertEqual(found.func, books_data)

	# Mengetes URL
	def search_url_exists(self):
		response = Client().get('search')
		self.assertEqual(response.status_code,200)

	# Mengetes URL menggunakan fungsi search()
	def using_search_func(self):
		found = resolve('search')
		self.assertEqual(found.func, search)

# class FunctionalTest(TestCase):

#     def setUp(self):
#     	chrome_options = Options()
#     	chrome_options.add_argument('--dns-prefetch-disable')
#     	chrome_options.add_argument('--no-sandbox')
#     	chrome_options.add_argument('--headless')
#     	chrome_options.add_argument('disable-gpu')
#     	service_log_path = "./chromedriver.log"
#     	service_args = ['--verbose']
#     	self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#     	self.selenium.implicitly_wait(25) 
#     	super(FunctionalTest,self).setUp()

#     def tearDown(self):
#     	self.selenium.quit()
#     	super(FunctionalTest, self).tearDown()

#     def test_html_element(self):
#     	selenium = self.selenium
# 	    # Opening the link we want to test
#     	selenium.get('https://situskeduadatul.herokuapp.com/books')

#     	# Waiting for loading circle
#     	time.sleep(10)

# 	    # find the form element
#     	searchButton = selenium.find_element_by_id('submit').get_attribute('value')

#     	# Checking Element
#     	self.assertIn('Search', searchButton)

#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('https://situskeduadatul.herokuapp.com/books')

#         # Waiting for loading circle
#         time.sleep(30)

#         # find the form element
#         searchQuery = selenium.find_element_by_id('search_input')
#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         searchQuery.send_keys('Machine')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)

#         # Checking the text
#         selenium.find_elements_by_xpath("//*[contains(text(), 'Machine')]")