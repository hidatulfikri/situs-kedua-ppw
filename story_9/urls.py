from django.urls import include, path
from .views import books, books_data, search

urlpatterns = [
	path('books', books, name='books'),
	path('api/books/', books_data, name='books_data'),
	path('search', search, name='search')
]