from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
import requests
import urllib.request, json

def books(request):
	return render(request, 'books.html')

def books_data(request):
	raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	itemss =[]
	for info in raw_data['items']:
		data = {}
		data['title'] = info['volumeInfo']['title']
		if 'authors' in info['volumeInfo']:
			data['author'] = ", ".join(info['volumeInfo']['authors'])
		else :
			data['author'] = ""
		data['publishedDate'] = info['volumeInfo']['publishedDate']
		itemss.append(data)
	return JsonResponse({"data" : itemss})

def search(request):
	if request.is_ajax():
		query = request.POST["q"]
		raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + query).json()
		itemss =[]
		for info in raw_data['items']:
			data = {}
			data['title'] = info['volumeInfo']['title']
			if 'authors' in info['volumeInfo']:
				data['author'] = ", ".join(info['volumeInfo']['authors'])
			else :
				data['author'] = ""
			if 'publishedDate' in info['volumeInfo']:
				data['publishedDate'] = info['volumeInfo']['publishedDate']
			else :
				data['publishedDate'] = ""
			itemss.append(data)
		return JsonResponse({"data" : itemss})