var counter =0;
var queryText ='';
$(document).ready(function() {
    $.ajax({
        type : 'GET',
        url : 'api/books',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
            for(var i=1; i<=data.data.length; i++) {
                print+= '<th scope ="row">' + i+'</th> <td>'+data.data[i-1].title+'</td> <td>';
                print+= data.data[i-1].author;
                print+= '</td> <td>' + data.data[i-1].publishedDate+'</td> <td>';
                print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';
            }
            $('tbody').append(print);
        },
        error: function() {
            console.log('fail');
        } 
    });
    $(document).on('click', '#button', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
        }
        $('.counters').html(counter);

    });
    /*
    $("search").submit( function(){
        var string = document.getElementById("search").q;
        $.ajax({
            type: "POST",
            url: "search",
            dataType: "json",
            data: {
                "query": string,
            },
            success : function(json) {
                console.log("Success");
                alert("Successfully sent the URL to Django");
            },
            error : function(xhr,errmsg,err) {
                console.log("Error");
                alert(string + "Could not send URL to Django. Error: " + xhr.status + ": " + xhr.responseText);
            }
        });
    });
    */
    $(function() {
        // Get the form.
        var form = $('#search');

        // Get the messages div.
        var formMessages = $('#form-messages');

        // TODO: The rest of the code will go here...
        // Set up an event listener for the contact form.
        $(form).submit(function(event) {
            // Stop the browser from submitting the form.
            event.preventDefault();

            // TODO
            // Serialize the form data.
            var formData = $(form).serialize();

            // Submit the form using AJAX.
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: formData
            })

            .done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            // Set the message text.
            $(formMessages).text(response);

            // Clear the form.
            $('#search_input').val('');
            })

            // Clear table
            $("#tbody").empty();

            // Repopulate table
            var print ='<tr>';
            for(var i=1; i<=data.data.length; i++) {
                print+= '<th scope ="row">' + i+'</th> <td>'+data.data[i-1].title+'</td> <td>';
                print+= data.data[i-1].author;
                print+= '</td> <td>' + data.data[i-1].publishedDate+'</td> <td>';
                print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';
            }
            $('tbody').append(print);

            .fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).text(data.responseText);
            } else {
                $(formMessages).text('Oops! An error occured and your message could not be sent.');
            }
            });
        });
    });
});
