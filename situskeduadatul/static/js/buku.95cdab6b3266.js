var counter =0;
var queryText ='';
$(document).ready(function() {
    $.ajax({
        type : 'GET',
        url : 'api/books',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
            for(var i=1; i<=data.data.length; i++) {
                print+= '<th scope ="row">' + i+'</th> <td>'+data.data[i-1].title+'</td> <td>';
                print+= data.data[i-1].author;
                print+= '</td> <td>' + data.data[i-1].publishedDate+'</td> <td>';
                print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';

            }
            $('tbody').append(print);
        },
        error: function() {
            console.log('fail');
        } 
    });
    $(document).on('click', '#button', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
        }
        $('.counters').html(counter);

    });

});
