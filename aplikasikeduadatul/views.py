from django.shortcuts import render
from .models import Status
from django.http import HttpResponseRedirect, JsonResponse
from .forms import FormStatus
import requests
import urllib.request, json

def index(request):
	if request.method == 'POST':
		form = FormStatus(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('')
		else:
			return HttpResponseRedirect('')
	else:
		form = FormStatus()
		daftarStatus = Status.objects.all()
		return render(request, 'index.html', {'form' : form, 'daftarStatus' : daftarStatus})

def hapusStatus(request):
	if request.method == 'POST':
		Status.objects.all().delete()
		return HttpResponseRedirect('')

def profil(request):
	return render(request, 'profil.html')

def buku(request):
	return render(request, 'buku.html')

def data_buku(request):
	raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	itemss =[]
	for info in raw_data['items']:
		data = {}
		data['title'] = info['volumeInfo']['title']
		if 'authors' in info['volumeInfo']:
			data['author'] = ", ".join(info['volumeInfo']['authors'])
		else :
			data['author'] = ""
		data['publishedDate'] = info['volumeInfo']['publishedDate']
		itemss.append(data)
	return JsonResponse({"data" : itemss})

def search(request):
	if request.is_ajax():
		query = request.POST["q"]
		raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + query).json()
		itemss =[]
		for info in raw_data['items']:
			data = {}
			data['title'] = info['volumeInfo']['title']
			if 'authors' in info['volumeInfo']:
				data['author'] = ", ".join(info['volumeInfo']['authors'])
			else :
				data['author'] = ""
			if 'publishedDate' in info['volumeInfo']:
				data['publishedDate'] = info['volumeInfo']['publishedDate']
			else :
				data['publishedDate'] = ""
			itemss.append(data)
		return JsonResponse({"data" : itemss})
	return render(request, 'buku.html')