from django.urls import include, path
from .views import index, hapusStatus, profil, buku, data_buku, search

urlpatterns = [
	path('', index, name='index'),
	path('hapus-status', hapusStatus, name='hapusStatus'),
	path('profil', profil, name='profil'),
	path('buku', buku, name='buku'),
	path('api/books/', data_buku, name='data_buku'),
	path('search', search, name='search')
]