from django import forms
from .models import Status

class FormStatus(forms.ModelForm):
    class Meta:
    	error_messages = {
    	'required': 'Tolong isi input ini',
    	}

    	model = Status
    	fields = ('cerita',)