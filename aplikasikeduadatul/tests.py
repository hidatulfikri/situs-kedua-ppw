from django.test import TestCase, Client
from django.urls import resolve
from .views import index, hapusStatus
from .models import Status
from .forms import FormStatus
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC  
from selenium.webdriver.common.by import By
import time

class SitusKeduaTest(TestCase):

	# TEST UNTUK INDEX.HTML
	# Mengetes URL
	def index_url_exists(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	# Mengetes URL menggunakan fungsi index()
	def using_index_func(self):
		found = resolve('')
		self.assertEqual(found.func, index)

	# Mengetes Models
	def using_status_model(self):
		# Membuat status baru
		Status.objects.create(teks='Apa kabarmu hari ini?')

		# Mengecek jumlah objek
		jumlahObjek = Status.objects.all().count()
		self.assertEqual(jumlahObjek, 1)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = FormStatus()
		self.assertIn('input', form.as_p())
    
	def test_form_validation_for_blank_items(self):
		form = FormStatus(data={'cerita':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors, {
			'cerita': ['This field is required.'],
			})
    
	def test_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('', {'cerita': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('', {'cerita': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	# TES UNTUK PROFIL.HTML
	# Mengetes URL
	def profil_url_exists(self):
		response = Client().get('profil')
		self.assertEqual(response.status_code,200)

	# Mengetes URL menggunakan fungsi profil()
	def using_profil_func(self):
		found = resolve('profil')
		self.assertEqual(found.func, profil)

class FunctionalTest(TestCase):

    def setUp(self):
    	chrome_options = Options()
    	chrome_options.add_argument('--dns-prefetch-disable')
    	chrome_options.add_argument('--no-sandbox')
    	chrome_options.add_argument('--headless')
    	chrome_options.add_argument('disable-gpu')
    	service_log_path = "./chromedriver.log"
    	service_args = ['--verbose']
    	self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    	self.selenium.implicitly_wait(25) 
    	super(FunctionalTest,self).setUp()

    def tearDown(self):
    	self.selenium.quit()
    	super(FunctionalTest, self).tearDown()

    def test_html_element(self):
    	selenium = self.selenium
	    # Opening the link we want to test
    	selenium.get('https://situskeduadatul.herokuapp.com')

    	# Waiting for loading circle
    	# element = WebDriverWait(selenium, 10).until(EC.invisibility_of_element_located((By.ID, 'status_title')))
    	time.sleep(5)

	    # find the form element
    	statusTitle = selenium.find_element_by_id('status_title').text
    	statusListTitle = selenium.find_element_by_id('status_list_title').text

    	# Checking Element
    	self.assertIn('Ada apa hari ini?', statusTitle)
    	self.assertIn('Daftar Cerita', statusListTitle)

    def test_css(self):
	    selenium = self.selenium
	    # Opening the link we want to test
	    selenium.get('https://situskeduadatul.herokuapp.com')
	    # find the form element
	    submitButton = selenium.find_element_by_id('submit')
	    eraseButton = selenium.find_element_by_id('erase_status')

	    # Checking CSS
	    self.assertIn('Mari cerita!', submitButton.get_attribute('value'))
	    self.assertIn('Hapus Semua', eraseButton.get_attribute('value'))

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://situskeduadatul.herokuapp.com')
        # find the form element
        status = selenium.find_element_by_id('id_cerita')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        # Checking the text
        selenium.find_elements_by_xpath("//*[contains(text(), 'Coba Coba')]")